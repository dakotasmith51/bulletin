## Homework 4

##### (Due Sunday 2/21/21 @ midnight)


This homework involves **2** repos.  Treat this one ("Homework 4") as usual: fork it (make your repo **private** _AND_ add me as a member of the project **with 'maintainer' permissions**.

#### Scoring

| Section | Points | 
|---------|--------|
| 1. Vote on a Distro | 5 |
| 2. Quiz			| 69 |
| 2a. TF				| 9 |
| 2b. Short-Answer	| 60 |
| 2c. Bonus			| 8 |
| **Total**  		| **74** |

---

#### 1. Vote on a Distro [5pts]

In this part, we are going to collaboratively edit a file... and use it to tally votes.
I've created a repo in the student area called 'shared'.  

- Clone (don't fork) that repo to your machine.
- `cd` into the repo 
- do a `git fetch`
- 
This will pull the most recent commits from the server and store them on your machine.
Doing a `git fetch` will not change anything in your working tree; all the updated data is kept away in the .git directory.

After you do a `git fetch`:
- do a `git status` to see if you are ahead/behind/up-to-date with respect to the remote repo.

Whenever someone else pushes their commits to the remote, you'll see that you've fallen behind and will need to catch up first before you can push your commits. To merge the new commits found during the `git fetch`, you can tell Git to merge the commits from the remote repo (named "origin" by default) into your working tree by running:

- `git merge` or `git merge origin/master`.

This merges the "master" branch (the default branch) of the remote repo called "origin" into your working tree. A repo can have several remotes; but in our case, we only have the one Git set up one for us when we first cloned the repo.  These two commands `git fetch` followed by `git merge` can be accomplished in one step by using `git pull` instead.

- open the file "voting.md" and cast your vote by **incrementing** the number next to the distro you want to vote for.

- save the file.
- make a commit which includes your change (`git commit voting.md -m "my vote"`)
- push your new commit to the remote (`git push`)

---


#### 2. Take a Short Quiz [69 pts]

True/False [3pts ea]:

1. We can save data in a format that doesn't consist of any bits.
> False, in digital computing, everything boils down to bits.

2. We can group bits together for interpretation any way we like.
> True, we can organize and manipulate information however we wish. Whether its digital representation is decipherable by another reader is another matter.

3. We can interpret a sequence of bits any way we like.
> Just as we can organize our information (encode) however we like, we can choose to interpret
> information in any way we wish.  Again, whether the result of decoding has any relationship to the original information is another matter.  An example of this is in HW2, when we used the `cat` command to print out the contents of the 	`multi.img` file.  Using `cat`, we implicitly chose to interpret the data as text, even though it was not actually text.  We are always free to mis-interpret data!


Exercises [4pts ea]:

1. What kind of file do you think `file1` is?
> It's an ASCII/UTF-8 encoded text file.

2. What is the value of the BYTE at address 0x4E in `file1` (expressed in hexadecimal)?
> Use of `xxd` reveals the value is 0x65.  It's the second to last BYTE in row 0x00000040.

3. If we assume ASII representation, how do we interpret that BYTE?
> `xxd` also shows us the ASCII interpretation in the column on the right side of its output.
> we see that byte 0x4E translates to the character "e".

4. If we assume 8-bit Unicode; how do we interpret that BYTE?
> UTF-8 and ASCII are equivalent, so that will also be "e".

5. If we assume 16-bit Unicode; how do we interpret the WORD at 0x4E-0x4F ?
> Look up the hex data stored at address 0x4E and 0x4F.  The result is 0x6576. Consult the Unicode tables on Wikipedia or Unicode.org and find the character encoded by 0x6576.  It is a CJK character: "敶".

6. If we assume that `file1` contains a single (unsigned) integer, what is the largest number we could store in the file (without increasing its size)?
> we can use the `ls` command to see the size of the file. It is 157 bytes long. That is 1256 bits.  If we use all 1256 bits to represent a single integer, then the largest integer we can represent is 2^1256 -1.

7. If we assume a sequence of integers (unsigned char) are stored back-to-back in `file2`, what numbers would we find stored in (decimal representation)?
> Use the internet to confirm the size of an "unsigned char" C data-type.  It is 8 bits, one byte.  `File2` consists of 10 bytes, so if each one is an integer, then we'd have 10 integers.
> Their decimal representation can be found by just converting each byte from hexadecimal to decimal. `xxd` can be used to see the hex representation of the file's contents.

8. If we assume a sequence of integers (unsigned short int) are stored in `file2`, what numbers would we find stored (in decimal)?
> Use the internet to confirm the size of an "unsigned short (int)" C data-type. It is 16 bits or 2 bytes.
> Because `file2` consists of 10 bytes, if we assume it is a sequence of unsigned short ints, then there would be 5 integers in the sequence.  We can view the hex representation with `xxd`, and then perform hexadecimal to decimal conversion of 2-byte chunks to obtain the 5 integers' decimal values.

9. What are the contents of the file called "thisisFile3insideHW3"  ?
> That file is empty.  We can confirm with the use of `ls -l` that its size is 0.

10. How do we know the name of the file "thisisFile3insideHW3"; in other words, where do we store the names of files?
> File names, file permissions, access times, and other metadata that we see by using `ls -l` or `stat` are all stored in the file system, not the file.

11. Create a new file called `myfile1` with the contents "this is my file"
> Because the text is in quotes, we know that it is meant to be a string representation.
> We want to create a text file which uses UTF-8 or ASCII encoding to store the string.
> You can do this on the terminal, from inside an interactive Python session, or with a python script.

12. Create a new file called `myfile2` with the contents "0x23"
> Here again, because the text is in quotes, we know that it is meant to be a string representation.
> We want to create a text file which uses UTF-8 or ASCII encoding to store the string.
> You can do this on the terminal, from inside an interactive Python session, or with a python script.

13. Create a new file called `myfile3` with the hex contents 0x23
> In this case, the value 0x23 is not in quotes, therefore we are to interpret it as "raw" hex data.
> We want to write the data into the file such that the file is one byte long, and the value of that byte is 0x23.
> You can do this on the terminal, from inside an interactive Python session, or with a python script.

14. Create a new file called `myfile4` with binary contents: 0b1101001111010000
> Once again, there are no quotes, and so we should interpret the binary data as raw data.
> We want to create a file that is 16 bits long (2 bytes). The value of the first byte is `0b11010011` or `0xD3` and the second byte is `0b11010000` or `0xD0`.
> You can do this on the terminal, from inside an interactive Python session, or with a python script.

15. Create a new file called `myfile5` with no contents
> here, we want to create a new file in the file system, but which has size 0.
> You can do this on the terminal, from inside an interactive Python session, or with a python script.

(Hints:  Read the manuals for `xxd`, `printf`, and `dd`; "0x" is the prefix which indicates a hex value; "0b" is a prefix which indicates a binary value)

**BONUS [8pts]**

Give your value for "X" from the lecture on 2/11. Give mathematical support.

> There is no real answer here.  In order to analytically come to a solution, we have to know _Exactly_ how many minor injuries equals one major injury.  Such an equivalence is arbitrary, subjective, and impossible to arrive at through science.
> You may make an assumption, and assert that "for this analysis we will assume that 100 minor injuries equals 1 major injury" and proceed from there.
> You may prefer to just say "there is no correct answer."