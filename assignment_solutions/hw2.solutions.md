# Homework 2 - Solutions


#### Scoring


| Part          | Points Avail |
|:--------------|:-------------:|
| Downloading   | 3     |
| .gitignore    | 3     |
| hashing       | 3     |
| head          | 3     |
| extract/zcat  | 6     |
| hash again    | 6     |
| losetup       | 6     |
| losetup -P    | 6     |
| FS info       | 6     |
| Mounting      | 10    |
| Unmounting    | 3     |
| **total**     | 55    |


#### [3 pts] Inspect Megi's Multiboot Image for the Pinephone:

##### Questions:

- What kind of file did you just download?
> we downloaded a disk "image", a byte-for-byte clone of a block storage device

- Why do we call it an "image"?
> Because it is a snapshot of an entire HDD or storage device, including partition table, partitions, and filesystems

- Why would Megi use this format to distribute it?
> Because it preserves all the particular nuances that the Pinephone requires for a bootable medium

---

#### [3 pts] Hashes:

##### Questions:
- How do the output of the last two commands compare?
> the result we get from computing the SHA256 hash should be the same as the one Megi publishes in the SHA256 file
- Why would MEGI distribute the `SHA256` file with the image?
> So that we can hash the image and compare our result to his; this ensures that nothing has been changed or corrupted from
> what he intended to distribute.

---

#### [3 pts] Look at the Multiboot image (download file):

##### Questions:
- Why does it look funny?  (use `head` to look at this .md file for comparison)
> it looks funny because the contents of the file are not valid ASCII or UTF-8 characters. "Head", "cat", and similar
> CLI tools expect text files and so interpret everything as though it were text.

---


#### [6 pts] Extract the actual image from what you downloaded

##### Questions:
- what does "`zcat`" mean (look it up)?
> "z" is commonly used to represent compressed data, so "zcat" is a tool meant to be used like "cat" but for compressed data, rather than text files.
- what happens after running this command?
> a new file is created with the decompressed contents
- what do the contents of the new file look like as compared to the *.gz version?
> the contents of the new file are also unintelligible, because, though decompressed, it is still not a text file


---

#### [6 pts] Deeper inspection of image file

##### Questions:

- what do we know about the decompressed file `multi.img` ?
> we know it has a DOS partition table

- what does it contain?
> it contains two partitions, the first is marked as bootable

- how did the output of the `sha256sum` command compare to the info in the `SHA256` file?
> our hash result should match the published version for the unzipped "multi.img" file


---


#### [6 pts] Recreate loopback device and scan


##### Questions:
- did we get a different response from `file` or `fdisk` when using the virtual (loopback) block device?
> when we setup loopback devices for the individual partitions, we didn't notice much different; When we used "-P" we saw that the second partition contains an EXT4 file system.

- is there anything suspicious about the output of either of those commands?
> it is suspicious that the first partition doesn't seem to contain a file system

- What happened after we ran `losetup -P ...`??  Why??
> seperate loopback devices were created for each partition.


---

#### [6 pts] Filesystem info (again)

##### Questions:

- what does the first partition contain?  Are there any files?
> unknown file system.  No file system, therefore no files.

- what does the second partition contain?  Are there any files?
> it contains an EXT4 file system; yes there are lots of files and directories

---

#### [10 pts] Mount the loopback devices

##### Questions:

- did these commands both succeed?
> no, we could not mount the first partition

- If not, which one failed and why?
> the second one failed

- what is the reason, do you think?
> it doesn't contain a file system

---

#### [3 pts] Unmount the loopback devices


##### Questions:

- where does the _real_ file system on the loopback device reside?
> all the contents of the loopback device (including the filesystems) reside inside the *.img file which is the source of the loopback.

- what happens to it after we unmount the device?
> we can no longer browse the filesystem from inside the OS
