# Homework 3
#### (due Sun, Feb 6 @ 12p)
 
In this homework, we will familiarize ourselves with the PinePhone.  We will first boot it the way it ships (your phones came with the KDE graphical user interface installed). Next, we will flash the `multi.img` image to the microSD card in your kit using the included microSD card reader. And finally, we will boot MEGI's P-boot, multiboot image, and explore some of the available graphical interfaces.

#### Scoring
| Section | Points |
|---------|--------|
| 1. Fork and clone | 5|
| 2. Boot phone | 10 |
| 3. Prepare uSD card with Megi's Muliboot image | 15|
|4. Insert Micro SD Card and boot it | 15 |
|**Total** | 45 |

---

#### 1. Gitlab Fork and `git clone` [5 pts()]
Like the other assignments, fork this one into your personal Gitlab.com namespace (make your forked project public), and then clone your fork to your local machine.

---

#### 2. Boot the PinePhone to factory image [10 pts]

Remove the PinePhone from the box. Follow the instructions at https://wiki.pine64.org/wiki/PinePhone#First_time_installation for removing the rear cover, removing the battery, and finally, removing the plastic insulator.

While the cover is off, notice the 6 exposed pogo-pin connectors and the row of switches. Make sure all your peripherals are turned on:

![img](https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/PinePhone_Kill_Interruptors_de_Maquinari_del_PinePhone_4529.jpg/320px-PinePhone_Kill_Interruptors_de_Maquinari_del_PinePhone_4529.jpg)

Put the battery back in, put the back cover back on, and press and hold the power button to boot the phone. (The power button is the lone button on the edge below the paired volume buttons.)

The default PIN to unlock the phones should be `123456` if that doesn't work, try `1111`.

Once you're booted to the main screen, use another cell phone or camera to take a picture of yourself with the PinePhone running KDE  :)

Put that photo in the hw3 repo.

##### Questions:

1. What was the hardest part of this step?
> no correct answer.

1. What kinds of thing would you want to connect the phone to to use the external interfaces?
> no correct answer


---


#### 3. Prepare the MicroSD card of MEGI's multiboot/p-boot image [15 pts]

- Run `sha256sum` on the `multi.img` file and check the output against MEGI's "SHA256" file to be sure it's not corrupted.

- Download "Rufus" (for Windows) or Balena Etcher (Linux VMs with graphics) to use to write the image to the SD card.

- insert the microSD card into the card reader, and insert the card reader into a USB port on your machine.

- Launch Rufus/Balena and write the `multi.img` image to the SDcard.  Students using Balena Etcher inside a VirtualBox machine will have to be sure that the machine has USB-passthrough enabled for the cardreader device.

- screenshot > "cardflash.png"

##### Questions:

1. Why couldn't we just mount the image and copy the files onto the microSD card?
> Because, we can only copy files to/from filesystems which are mounted.  We know from HW2 that the first partition (boot partition) does not have a filesystem.

---


#### 4. Insert the microSD card into the PinePhone and boot it! [15 pts]

1. Once again, remove the back of the PinePhone.
    - You'll have to take out the battery to get clear access to the microSD card slot.
    - **Note: the microSD card slot is directly above the SIM card slot. **

	<img src="https://wiki.pine64.org/images/thumb/6/61/Pinephone_backside.png/400px-Pinephone_backside.png" alt="back of phone" width=200>

2. After inserting the microSD card, put the back cover back on and (long?)-press the power button to boot the phone. You should see something like this:

	<img src=https://xnux.eu/p-boot-demo/menu2.webp alt="drawing" width=200>

3. Use the volume keys to move up and down and select a Distro (distribution) to boot.  Try as many as you like. Passwords are either `1111` or `123456`

4. (photo) use another camera to take at least two photos of two different distros (other than KDE) and put those photos in your hw3 repo.

5. Try to use the PinePhone's camera from inside of each OS

#### Questions:

1. there are two Manjaros.  Why?  What is the difference?
> The two Manjaros (OS) make use of two different graphical front-ends (also called "desktop environments")

1. What is Phosh?
> Phosh is a graphical front end (desktop environment) created and maintained by the Mobian developers.

1. What is Plasma?
> Plasma is a graphical front end (desktop environment) created and maintained by the KDE developers.

1. Was the quality/success with using the camera consistent across the distros you tried?
> No correct answer, but obviously there is variance. Some don't come with camera apps, and some don't work.